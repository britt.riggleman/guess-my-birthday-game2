from random import randint

# Prompt the person for their name using the input function
name = input("Hi, Friend! What is your name? ")

# Create 3 variables-- a variable for first guess number,
# a variable for the month number set range + year number set range
for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    # Now tell the computer to make it's first guess + ask a question
    print("Guess", guess_number, "were you born in",
        month_number, "/", year_number, "?")

    # Get input to check if computer is correct with their guess
    response = input("yes or no? ")

    # Use the if and else statements --
    # If user types yes, print I knew it! Else, print Drat! I'll try again
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have better things to do. Goodbye!")
    else:
        print("Dang! Let me try again!")
 # Use a for statement to help simplify your code!
